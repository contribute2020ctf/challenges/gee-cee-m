json.extract! user, :id, :name, :token, :message, :created_at, :updated_at
json.url user_url(user, format: :json)
