class TokenController < ApplicationController
  include CryptoHelper
  def redeem
     decrypted = aes256_gcm_decrypt(params[:token])
     if decrypted == "caughtthetanuki"
       render plain: ENV["FLAG2"]
     else
       render plain: "noot"
     end
     
  end
end
