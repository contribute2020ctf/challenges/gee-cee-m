module CryptoHelper
  extend self

  AES256_GCM_OPTIONS = {
    algorithm: 'aes-256-gcm',
    key: Rails.application.credentials.aes_key,
    iv: Rails.application.credentials.aes_iv
  }.freeze

  def aes256_gcm_encrypt(value)
    encrypted_token = Encryptor.encrypt(AES256_GCM_OPTIONS.merge(value: value))
    Base64.strict_encode64(encrypted_token)
  end

  def aes256_gcm_decrypt(value)
    return unless value

    encrypted_token = Base64.decode64(value)
    Encryptor.decrypt(AES256_GCM_OPTIONS.merge(value: encrypted_token))
  end

end

class EncryptionWrapper
include CryptoHelper
  def before_save(record)
    record.token = aes256_gcm_encrypt(record.token)
  end

  def after_save(record)
    record.token = aes256_gcm_decrypt(record.token)
  end

  alias_method :after_initialize, :after_save
end
