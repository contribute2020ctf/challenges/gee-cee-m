Rails.application.routes.draw do
  resources :users, only: [:index, :new, :show, :create]
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get 'redeem/:token', to: 'token#redeem'
end
